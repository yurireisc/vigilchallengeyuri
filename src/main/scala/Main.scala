import java.io.FileNotFoundException
import java.util.Properties
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.io.StdIn.readLine

object Main {

  def readValues() : (String, Int) = {
    var data = "";
    var delimiter = 0;

    println("Type the text: ");
    data = readLine();

    println("Inform the delimiter");
    delimiter = readLine().toInt;

    return (data, delimiter);
  }

  @tailrec def nextCut(data: String, index: Int): Int = {
    if(data.charAt(index) == ' ' || index < 0){
      return index;
    }else{
      return nextCut(data, index - 1);
    }
  }

  def positionstoCut(data: String, delimiter: Int): List[Int] = {
    var positionstoCut : ListBuffer[Int] = ListBuffer.empty;
    var x = delimiter;
    while (x < data.length){
      var next = nextCut(data, x);
      positionstoCut += next;
      x = next;
      x += delimiter;
    }
    return positionstoCut.toList;
  }

  def main(args: Array[String]): Unit = {

    var (data, delimiter) = readValues();

    var postoCut = positionstoCut(data, delimiter);

    var (remaining, toPrint) = postoCut.foldRight((data, List[String]())){
      case (current, (a,toPrt)) =>
        val (remaining, cut) = a.splitAt(current);
        (remaining, cut.trim :: toPrt);
    }

    var split = remaining::toPrint;

    split.foreach(l => println(l))

  }
}